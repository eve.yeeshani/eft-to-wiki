from os import listdir
from os.path import isfile, join
import json
import requests

eftsections = ["low", "mid", "high", "rig", "subsystem", "drone",
               "charge", "charge", "charge", "charge", "charge", "charge"]

wikisections = ["low", "mid", "high", "rig", "subsystem", "drone", "charge"]

class Ship:
    def __init__(self, name, type_name):
        self.name = name
        self.type_name = type_name
        self.data = {}
        self.ids = {}

    def add_data(self, data_type, data):
        if data_type not in self.data:
            self.data[data_type] = []
        self.data[data_type].append(data)
        self.ids[extract_type(data)] = -1

    def __str__(self):
        return f"{self.name}({self.type_name}):\n{self.data}\n{self.ids}"


def extract_type(line: str):
    ammount_pos: int = line.find(" x")
    itype = line
    if ammount_pos > 0:
        itype = line[0: ammount_pos].strip()
    return itype


def read_file(file):
    file = open(file, "r")
    header = file.readline().strip()
    if not header.startswith("[") or not header.endswith("]"):
        raise Exception("must start with header [shiptype, name]")
    seperatorpos = header.find(",")
    if seperatorpos < 0:
        raise Exception("cant read header header [shiptype, name]")
    shiptype = header[1:seperatorpos].strip()
    shipname = header[seperatorpos+1:-1].strip()

    ship = Ship(shipname, shiptype)
    print(f"-name: '{shipname}' type:'{shiptype}'")

    section = 0
    section_count = 0
    skipped_section = False
    lastwasheader = True
    for line in file:
        curr = line.strip()
        if not curr:
            if lastwasheader:
                lastwasheader=False
                continue
            #if section_count == 0 and not skipped_section:
            #    skipped_section = True
            #    continue
            skipped_section = False
            print(f"-end {eftsections[section]}")
            section += 1
            section_count = 0
            continue
        lastwasheader = False
        section_count += 1
        ship.add_data(eftsections[section],curr)
        print(f"-{eftsections[section]} {section_count}:{curr}")
    file.close()
    return ship


def add_ids(ship):
    url = 'https://esi.evetech.net/latest/universe/ids/'
    y = requests.post(url, data=json.dumps(list(ship.ids.keys())))
    z = json.loads(y.text)
    print(z)
    for itemtype in z["inventory_types"]:
        ship.ids[itemtype["name"]] = itemtype["id"]
    return ship


def to_wiki_string(ship):
    ret_val = "{{ShipFitting\n"
    ret_val += f"|ship= {ship.type_name}\n"
    ret_val += f"|fitName= {ship.name}\n"
    ret_val += f"|fitID= {(''.join(ship.name.split()))}\n"
    for section in wikisections:
        sectioncount = 0
        if section not in ship.data:
            continue
        for item in ship.data[section]:
            sectioncount +=1
            ret_val += f"|{section}{sectioncount}name={item}\n"
            ret_val += f"|{section}{sectioncount}typeID={ship.ids[extract_type(item)]}\n"

    ret_val += "| difficulty=0\n"
    ret_val += "| version=0\n"
    ret_val += "| skills=\n"
    ret_val += "| notes=\n"
    ret_val += "| showTOC=N\n"
    ret_val += "| showSKILLS=N\n"
    ret_val += "| showNOTES=N\n"
    ret_val += "| alphacanuse = N\n"
    ret_val += "}}\n"
    return ret_val


def write_file(file, text):
    file = open(file, "w")
    file.write(text)
    file.close()


if __name__ == '__main__':
    extension = "eft.txt"
    onlyfiles = [f for f in listdir(".") if isfile(join(".", f))]
    for file in onlyfiles:
        if file.endswith(extension):
            ship = read_file(file)
            ship = add_ids(ship)
            outstring = to_wiki_string(ship)
            #print(outstring)
            tgt = file[0: -1*len(extension)]
            tgt += "wiki.txt"
            write_file(tgt, outstring)







